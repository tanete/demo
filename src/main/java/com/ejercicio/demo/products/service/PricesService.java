package com.ejercicio.demo.products.service;

import com.ejercicio.demo.products.service.domain.ProductPriceBo;

import java.util.Date;

public interface PricesService {

    ProductPriceBo getPrice(Date applicationDate, Integer productId, Integer brandId);
}
