package com.ejercicio.demo.products.service.domain;

import com.ejercicio.demo.products.repository.entity.Prices;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class ProductPriceBo {

    private Integer productId;

    private Integer brandId;

    private Integer priceList;

    private Date applicationStartDate;

    private Date applicationEndDate;

    private Float price;

    public ProductPriceBo(Prices prices){
        this.productId = prices.getProductId();
        this.brandId = prices.getBrandId();
        this.priceList = prices.getPriceList();
        this.applicationStartDate = prices.getStartDate();
        this.applicationEndDate = prices.getEndDate();
        this.price = prices.getPrice();
    }
}
