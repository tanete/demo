package com.ejercicio.demo.products.service.impl;

import com.ejercicio.demo.products.repository.PricesRepository;
import com.ejercicio.demo.products.service.PricesService;
import com.ejercicio.demo.products.service.domain.ProductPriceBo;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PricesServiceImpl implements PricesService {

    private final PricesRepository pricesRepository;

    public PricesServiceImpl(PricesRepository pricesRepository) {
        this.pricesRepository = pricesRepository;
    }

    @Override
    public ProductPriceBo getPrice(Date applicationDate, Integer productId, Integer brandId) {
        return new ProductPriceBo(pricesRepository.getPrice(applicationDate,productId,brandId));
    }
}
