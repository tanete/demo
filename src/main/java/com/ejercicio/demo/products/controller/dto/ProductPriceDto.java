package com.ejercicio.demo.products.controller.dto;

import com.ejercicio.demo.products.service.domain.ProductPriceBo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class ProductPriceDto {

    private Integer productId;

    private Integer brandId;

    private Integer priceList;

    private Date applicationStartDate;

    private Date applicationEndDate;

    private Float price;

    public ProductPriceDto(ProductPriceBo prices){
        this.productId = prices.getProductId();
        this.brandId = prices.getBrandId();
        this.priceList = prices.getPriceList();
        this.applicationStartDate = prices.getApplicationStartDate();
        this.applicationEndDate = prices.getApplicationEndDate();
        this.price = prices.getPrice();
    }
}
