package com.ejercicio.demo.products.controller;

import com.ejercicio.demo.products.controller.dto.ProductPriceDto;
import com.ejercicio.demo.products.service.PricesService;
import com.ejercicio.demo.products.service.domain.ProductPriceBo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/prices")
public class PricesController {

    private final PricesService pricesService;

    public PricesController(PricesService pricesService){
        this.pricesService = pricesService;
    }

    @GetMapping
    public ResponseEntity<ProductPriceDto> getPrice(
            @RequestParam(value = "applicationDate", required = true) Date applicationDate,
            @RequestParam(value = "productId", required = true) Integer productId,
            @RequestParam(value = "brandId", required = true) Integer brandId){
        ProductPriceBo serviceResult = pricesService.getPrice(applicationDate, productId, brandId);
        ProductPriceDto result = new ProductPriceDto(serviceResult);
        return  ResponseEntity.ok().body(result);
    }
}