package com.ejercicio.demo.products.repository;

import com.ejercicio.demo.products.repository.entity.Prices;

import java.util.Date;

public interface PricesRepository{

    Prices getPrice(Date applicationDate, Integer productId, Integer brandId);
}