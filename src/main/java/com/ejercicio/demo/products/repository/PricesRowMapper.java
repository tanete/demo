package com.ejercicio.demo.products.repository;

import com.ejercicio.demo.products.repository.entity.Prices;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PricesRowMapper implements RowMapper<Prices> {

    @Override
    public Prices mapRow(ResultSet rs, int rowNum) throws SQLException {
        Prices prices = new Prices();
        prices.setProductId(rs.getInt("PRODUCT_ID"));
        prices.setBrandId(rs.getInt("BRAND_ID"));
        prices.setPriceList(rs.getInt("PRICE_LIST"));
        prices.setStartDate(rs.getDate("START_DATE"));
        prices.setEndDate(rs.getDate("END_DATE"));
        prices.setPrice(rs.getFloat("PRICE"));
        return prices;
    }
}
