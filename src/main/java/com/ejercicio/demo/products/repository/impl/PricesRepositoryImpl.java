package com.ejercicio.demo.products.repository.impl;

import com.ejercicio.demo.products.repository.PricesRepository;
import com.ejercicio.demo.products.repository.PricesRowMapper;
import com.ejercicio.demo.products.repository.entity.Prices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class PricesRepositoryImpl implements PricesRepository {

    private final String query ="SELECT p.product_id, p.brand_id, p.price_list, p.start_date, p.end_date, p.price " +
            "FROM Prices as p " +
            "JOIN Brand as b ON (b.id = p.brand_id) " +
            "WHERE :applicationDate BETWEEN p.start_date AND p.end_date " +
            "AND p.product_id = :productId " +
            "AND p.brand_id = :brandId " +
            "ORDER BY p.priority DESC " +
            "LIMIT 1";

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Prices getPrice(Date applicationDate, Integer productId, Integer brandId) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("applicationDate", applicationDate)
                .addValue("productId", productId).addValue("brandId", brandId);
        return jdbcTemplate.queryForObject(query, namedParameters, new PricesRowMapper());
    }
}
