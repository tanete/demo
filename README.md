# Ejercicio 

El endpoint expuesto en /api/prices resuelve el ejercicio propuesto (enunciado anexado al final) y recibe 3 parametros:
-applicationDate de tipo Date al que se le debe enviar los valores en formato "yyyy/mm/dd hh:mm:ss"
-productId de tipo Integer
-brandId de tipo Integer

Para realizar el test pueden ejecutarse los test provistos o a traves de alguna herramienta de API test como Postman.

Ejemplo de Json para realizar test en Postman:
{
	"info": {
		"_postman_id": "76c6c8fa-84da-48ec-b1c4-8d1eeed3e68e",
		"name": "Ejercicio",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "getPrices",
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": ""
				},
				"url": {
					"raw": "http://localhost:8080/api/prices?applicationDate=2020/06/14 16:00:00&productId=35455&brandId=1",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "8080",
					"path": [
						"api",
						"prices"
					],
					"query": [
						{
							"key": "applicationDate",
							"value": "2020/06/14 16:00:00"
						},
						{
							"key": "productId",
							"value": "35455"
						},
						{
							"key": "brandId",
							"value": "1"
						}
					]
				}
			},
			"response": []
		}
	]
}

Para acceder a la Db en memoria se debe cargar en el navegador la siguiente url: http://localhost:8080/h2/

Enunciado original:

En la base de datos de comercio electrónico de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto de una cadena entre unas fechas determinadas. 
A continuación se muestra un ejemplo de la tabla con los campos relevantes:
 
PRICES
-------
 
BRAND_ID         START_DATE                    END_DATE               PRICE_LIST      PRODUCT_ID        PRIORITY         PRICE           CURR
------------------------------------------------------------------------------------------------------------------------------------------------------
1         2020-06-14 00.00.00             2020-12-31 23.59.59             1             35455              0             35.50            EUR
1         2020-06-14 15.00.00             2020-06-14 18.30.00             2             35455              1             25.45            EUR
1         2020-06-15 00.00.00             2020-06-15 11.00.00             3             35455              1             30.50            EUR
1         2020-06-15 16.00.00             2020-12-31 23.59.59             4             35455              1             38.95            EUR
 
Campos: 
 
BRAND_ID: foreign key de la cadena del grupo (1 = Tienda).
START_DATE , END_DATE: rango de fechas en el que aplica el precio tarifa indicado.
PRICE_LIST: Identificador de la tarifa de precios aplicable.
PRODUCT_ID: Identificador código de producto.
PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor prioridad (mayor valor numérico).
PRICE: precio final de venta.
CURR: iso de la moneda.
 
Se pide:
 
Construir una aplicación/servicio en SpringBoot que provea una end point rest de consulta tal que:
 
- Acepte como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena.
- Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.
 
Se debe utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo, 
(se pueden cambiar el nombre de los campos y añadir otros nuevos si se quiere, elegir el tipo de dato que se considere adecuado para los mismos).
              
Desarrollar unos test al endpoint rest que  validen las siguientes peticiones al servicio con los datos del ejemplo:
                                                                                       
-          Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (Tienda)
-          Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (Tienda)
-          Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (Tienda)
-          Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (Tienda)
-          Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (Tienda)
 
 
Se valorará:
 
Diseño y construcción del servicio.
Calidad de Código.
Resultados correctos en los test.

